The repository has been moved to GitHub:
https://github.com/bugzinga/redcase

We had been using Bitbucket's free tier to host our repositories for about 5 years, but on May 13, 2017, we have received the following email from Atlassian:

---
**Subject**: Your Bitbucket (Cloud) account is expiring

Hello %USER%,

This is just a heads up to let you know that your Bitbucket (Cloud) account is about to expire. We have noticed that you have opted for auto-renewal of your account but have not provided us with your credit card details.

Your current subscription details are as follows:
```
Account: %ACCOUNT% (SEN-%ID%)
Product: Bitbucket (Cloud)
Expiry Date: 15 May 2017
Amount: US $10.00
```

If you wish to continue using Bitbucket (Cloud), please provide us with your credit card details via your My Atlassian account prior to the expiry date.

If we do not receive your credit card details when your account expires, your Bitbucket (Cloud) account will automatically be deactivated. Your data will be purged shortly after and you will no longer be able to restore your data.

Please contact us with any questions, we're always happy to help.

Cheers,
The Atlassians
---

We have decieded that this notice doesn't allow us to trust Atlassian anymore nor use the services the company provides, therefore, we have moved all our repositories to GitHub.

Bugzinga Team

May 14, 2017
